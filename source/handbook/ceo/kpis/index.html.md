---
layout: markdown_page
title: "KPIs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What are KPIs

Every department at GitLab has Key Performance Indicators (KPIs).
Avoid the term metric where we can be more explicit. Use KPI instead.
A department's KPIs are owned by the respective member of e-group.

The KPI definition should be in the most relevant part of the handbook which is organized by [function and results](/handbook/handbook-usage/#style-guide-and-information-architecture).
In the definition, it should mention what the canonical source is for this indicator.
Where there are formulas include those as well.
Goals related to KPIs should co-exist with the definition.
For example, "Wider community contributions per release" should be in the Community Relations part of the handbook and "Average days to hire" should be in the Recruiting part of the handbook.

## List of KPIs

The data team maintains [a list of GitLab KPIs and links to where they are defined](/handbook/business-ops/data-team/metrics/).

## Public KPIs

In the doc 'GitLab Metrics at IPO' are the KPIs that we may share publicly.

## Parts of a KPI

A KPI or metric consists of multiple things:

1. Definition: how we calculate it
1. Goal: what we strive for
1. Plan: what we have in our yearly plan
1. Commit: the most negative it will be
1. 50/50: the median estimate, 50% chance of being lower and higher
1. Best case: the most positive it will be
1. Forecast: what we use in our rolling 4 quarter forecast
1. Actual: what the number is

We have public goals for all KPIs but we don't have public actuals for most.
