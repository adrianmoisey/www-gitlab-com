---
layout: markdown_page
title: "Okta"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What is Okta?

From the Okta website - Okta is the foundation for secure connections between people and technology. It’s a service that gives employees, customers, and partners secure access to the tools they need to do their most important work.

In practice - Okta is an Identity and Single Sign On solution for applications and Cloud entities. It allows GitLab to consolidate authentication and authorisation to Applications we use daily through a single dashboard and ensure a consistent, secure and auditable login experience for all our staff.

## How is GitLab using Okta?

GitLab is using Okta for a few key goals :

- We can use Okta to enable Zero-Trust based authentication controls upon our assets, so that we can allow authorised connections to key assets with a greater degree of certainty.
- We can better manage the login process to the 80+ and growing cloud applications that we use within our tech stack.
- We can better manage the Provisioning and Deprovisioning process for our users to access these application, by use of automation and integration into our HRIS system.
- We can make Trust and Risk based decisions on authentication requirements to key assets, and adapt these to ensure a consistent user experience.


## What are the benefits to me using Okta as a user?

- A single Dashboard that is provided to all users, with all the applications you need in a single place. 
- Managed SSO and Multi-Factor Authentication that learns and adapts to your login patterns, making life simpler to access the assets you need. 
- Transparent Security controls with a friendly User Experience. 

## What are the benefits to me as an application administrator to using Okta?

- Automated Provisioning and Group Management
- Ability to transparently manage shared credentials to web applications without disclosing the credentials to users
- Centralised access for users, making it easy to add, remove and change the application profile without the need to update all users. 

## How do I get my Okta account set up?

GitLab is running an Open Beta through April and May 2019 - please sign up using [the Okta Beta signup form](https://forms.gle/6YHwYveJRipygS5r9). 

After May 2019, all GitLab team-members will have an Okta account set up as part of their onboarding process. 

Follow the GitLab Okta [Getting Started Guide](https://docs.google.com/document/d/1x2NJan0job5nM5tT8HF6yofg-Y2aAsSVKc6qNnCuoxo/) and [FAQs](https://docs.google.com/document/d/1fRiWDmZd3BNu7dkr40h0eAWtE1KW5TifPRYXXUKh2a0/).

## How do I get my Application set up within Okta?

If you are an Application Owner please submit an [new application setup issue](https://gitlab.com/gitlab-com/gl-security/zero-trust/okta/issues/new) on the Okta project page for your application. We will work with you to verify details and provide setup instructions. 


## Where do I go if I have any questions?

- **Slack** `#okta-openbeta` Channel
- [Okta Beta Comments issue](https://gitlab.com/gitlab-com/gl-security/zero-trust/okta/issues/2)


