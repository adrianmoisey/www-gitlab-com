---
layout: markdown_page
title: "Technical Evangelism"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Goal
To build GitLab’s technical brand with deep, meaningful conversations on engineering topics relevant to our industry by leveraging our community of team-members and the wider ecosystem.

### Evolution of technical evangelism at GitLab
**1.0 - (The past)** Developer advocates/technical evangelists went and gave talks <br>

**2.0 - (Today)** Team-members from alliances, marketing, product, engineering evangelize and give talks <br>

**3.0 (To come)** - The wider GitLab community becomes evangelists and gives talks, creates content, and shares their successes with GitLab with the wider ecosystem. The ideal ratio of team-members to the wider community members should be 20:80. Currently we are more like 80:20. Our challenge is to solve for this while ensuring our presence in key industry events and online forums.

### Why push the wider community?
We are aiming for 3.0 for one big reason: **Leverage**. 

1. Top tier events: There is an inordinate amount of value from a presence at the most valued technology events that attract thousands or are the most revered. In such places, we need to show off our technical chops at GitLab and present our point of view in the industry. At the same time, without the wider community present there to echo the same message, our story will fall on deaf ears. 
2. Beyond the top tier of conferences, we are better served by activating our network of speakers across the community than we are by hosting booths. This is because we can get deeper reach within local/specialized communities and also target a wider audience with a wider community effort. 


<br>Meghan Gill, former developer marketing leader at MongoDB and current VP of Sales Ops there, is a champion of [leveraging the community](http://web.archive.org/web/20120625174339/http://meghangill.com:80/2012/06/11/why-invest-in-communit-leaders/). The reasons she gives are:

* The psychological reason: Messages about a product are always more powerful coming from a user.
* The economic reason: By investing in a community member and helping them become a great evangelist, we spend once and continually reap benefits. However, when we send team-members to conferences, we take time away from their daily jobs slowing down product development. Each time a team member leaves their desk, there is an opportunity cost which we do not have to pay with a community member. 
* The sustainability reason: Once we enable community members to have a voice in the ecosystem, there is a snowball effect and they get invited to present their point of view repeatedly. Our investment is largely done but the benefit keeps coming back. 
* The amplification reason: There is no way one company can reach everyone. However, with community evangelists in place, they can amplify the reach beyond a company’s corridors of influence. 




