---
layout: markdown_page
title: "Analyst Relations"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Analyst relations at GitLab

Analyst relations (AR) is generally considered to be a corporate strategy, communications and marketing activity, but at GitLab, because of our mission that everyone can contribute, we view the analyst community as participants as well. The primary owner of Analyst Relations at GitLab is Product Marketing, as they are the conduit between the analysts and the internal company information.  

Industry analysts usually focus on corporate or organizational buyers and their needs rather than on those of individual developers. At GitLab we view them as a marketing focus for those markets. They amplify our message to a different group than our traditional developer communities, although there is overlap to some degree between those communities.

## How we interact with the analyst community

Examples of how we engage with analysts include:
- Schedule briefings where we update analysts on our product, our company, our future direction and strategy;
- Answer questions for analyst reports such as Gartner MQs or Forrester Waves that provide in-depth information on product features, customer references and company information;
- Use analyst reports such as Gartner MQs or Forrester Waves that feature GitLab to help enhance or clarify our story for customers and partners;
- Provide our analyst newsletter to update analysts on what GitLab is doing between briefings;
   - [GitLab Analyst Newsletter October 2018](https://docs.google.com/document/d/1MrAsx2UgIQjwbIEplrqvUvZajEzVRuKfZOGhU-qTExw/edit?usp=sharing)
- Schedule inquiries where analysts answer specific questions we have about products, markets, or strategies we want to understand better;
- Schedule consulting days for an extended dive with an analyst into products, markets, or strategies we are working on;
- Invite analysts to participate in webinars, speaking engagements, quotes for media, or other events where an analyst presence would be beneficial; and
- Hire analyst’s market research departments to help us create, run, and interpret survey research that helps us target markets or develop products optimally.

## How we on-board a new analyst to the GitLab Analyst Relations community

- Open an issue in the Product Marketing project using the Analyst On-boarding template that details:
  - who they are
  - best fit for coverage
  - plan for briefing or an introductory inquiry
- Set them up in ARInsights, the third party analyst-tracking software we use
- Add them to the appropriate analyst newsletter list
- Add them to the forthcoming analyst heat map

## How we off-board an analyst from the GitLab Analyst Relations community

- Open an issue in the Product Marketing project using the Analyst Off-boarding template that details:
  - Highlight their change of coverage area or
  - Identify their reason for off-boarding
  - Identify who is their short-term/long-term replacement
  - Notify the PM/PMM team of transition and change to relationship

## Responding to vendor product comparison reports (e.g. MQs or Waves)

- GitLab will participate in any Wave or MQ to which we are invited to participate. We will commit to our best possible answers for the questionnaires.
- The parts of the team responsible for answering the questionnaire will make this a priority. We will:

   - Present every feature in the best possible light so we have the most defensible chance at high scores.
   - Provide whatever evidence we can that even hints at what the analysts are looking for.
   - When responding to the analyst request, challenge ourselves to find a way to honestly say “yes” and paint the product in the best light possible. Often, at first glance if we think we don’t support a feature or capability, with a bit of reflection and thought we can adapt our existing features to solve the problem at hand.
   - Even if we don't score well on the product today, we *must* score well on strategy.

- Once the report is published, we will create a microsite for each report that details the strengths and weaknesses and provides a link to relevant epics and issues that show how we are working to improve areas mentioned in the report. This becomes the basis for responding to the next iteration of that report, to continue the dialogue with the analysts, as well as to establish or demonstrate our thought leadership or go-to-market in that space.

## Process for responding to vendor product comparison reports (e.g. MQs or Waves)

#### Before the questionnaire arrives:

- GitLab usually receives notice 1-2 weeks in advance that a Wave or MQ is forthcoming, in which they can participate.
    - AR works with PMM team to determine lead PMM and lead PM for the project.
    - AR notifies Customer Reference that a request is imminent.

### When the questionnaire arrives:

- AR replies to analyst firm, acknowledging receipt of invitation and agreement to participate, and indicates key participants (AR Manager and lead PMM.)
- AR responsibilities:
    - create an issue for the report, a place to keep all links, due dates, supporting materials, related issues, and overall conversation
    - create a slack channel dedicated to this report, inviting all relevant members to the channel
    - forward any Customer Reference material to the CR Manager
    - send out an invite for any kickoff call that the analyst may run related to this report.
    - coordinate all activities for deadlines
    - manage all correspondence between the analyst company and GitLab, including submission of final questionnaire and customer references
    - coordinate with team for demo/briefing time slot
    - work with finance for company information
    - support the PMM where necessary

- PMM responsibilities:
    - read all informataion provided by AR in the issue
    - attend all meetings with the analysts related to this report
    - own responsibility for coordinating, collating, and organizing all input requirements including:
        - draft questionnaire feedback
        - final questionnaire response
        - briefing presentation to analysts
        - demo for analysts
        - follow up questions for analysts
    - drive the briefing to the analysts
    - coordinate with Product leadership/CEO others on final version of the questionnaire   

- Technical product marketing responsibilities:
    - read all information provided by AR in the issue
    - attend all meetings with the analysts related to this report
    - work with PMM and PM in creation of demo and briefing
    - drive the demo during the demo and briefing time slot

- PM responsibilities
    - read all informataion provided by AR in the issue
    - attend all meetings with the analysts related to this report
    - provide feedback to PMM on draft questionnaire
    - place the questionnaire and briefing/demo at the top of the work priority list
    - provide first pass answers to the questionnaire to PMM
    - work with PMM to finalize all answers to the questionnaire

### When the draft report arrives:

- AR shares the draft report with the team to check for technical accuracy
- PMM takes lead in verifying/correcting technical accuracy
- PM provides corrections for technical accuracy
- AR works with PMM for company accuracy issues
- AR submits final corrected version to analysts

- PMM writes blog post on the report - optional - decided on a case by case basis - to be released when the report is published
- PMM and AR create messaging together for Sales - what is the story around this report - to be released when the report is published


### When the report is published:

- AR makes the report available internally and notifies PMM/Product/Sales
  - possibility of reprints - depending on placement and/or campaign needs
  - creates microsite for the report
  - creates an issue for the microsite, and assigns PMM/PM to add relevant information on issues or epics pertaining to the issues raised by the analysts
  - sets up an inquiry with the analyst company to discuss the results of the report and answer questions
  - works with marketing to set up forms and tracking if reprints are chosen.


## Accessing analyst reports

Most analyst companies charge for access to their reports.

-   If GitLab purchases reprint rights to a report, then that link will be available here, on the [Analyst Relations web page](https://about.gitlab.com/analysts/), and on the relevant product page. Reprint rights are the rights to share the link to the report - these generally last six months to one year.
-   GitLab maintains relationships with some analyst companies that provide us with access to some or all of their relevant research. These reports are for internal use only and sometimes limited to named individuals.  Those reports are generally kept in an internal [GitLab folder for analyst relations](https://drive.google.com/drive/u/0/folders/1oFmtmoXsbjMb6IuPIgIIZ-MG-tLfOjpw). If you are a GitLab team-member and you need access to a particular report, please reach out to [Analyst Relations](mailto:analysts@gitlab.com) and I'll help you find the research you need.

## What the analysts are saying about GitLab

This section contains highlights from areas where the analysts have rated GitLab in comparison to other vendors in a particular space. The highlights and lessons learned are listed here.  For more information click the link to go to the page dedicated to that report.

##### [The Forrester New Wave™: Value Stream Management Tools, Q3 2018](https://about.gitlab.com/analysts/forrester-vsm/)
##### [The Gartner Magic Quadrant for Application Release Orchestration, 2018](https://about.gitlab.com/analysts/gartner-aro/)

## Analyst reports that can help you deepen your knowledge
As part of XDR and Sales Enablement, some analyst reports are educational - they can help you build your understanding of the market. We've collected some of those articles to share with you here. These are behind the firewall and for use of employees where they have access rights. If you have any questions on access, please contact [Analyst Relations](mailto:analysts@gitlab.com).

[The complete list of documents by topic can be found here.](https://about.gitlab.com/handbook/marketing/product-marketing/analyst-relations/sales-training/)
