---
layout: markdown_page
title: Handbook
twitter_image: '/images/tweets/handbook-gitlab.png'
---

The GitLab team handbook is the central repository for how we run the company. Printed, it consists of over [2,000 pages of text](/handbook/tools-and-tips/#count-handbook-pages). As part of our value of being transparent the handbook is <a href="https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/handbook">open to the world</a>, and we welcome feedback<a name="feedback"></a>. Please make a <a href="https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests">merge request</a> to suggest improvements or add clarifications.
Please use <a href="https://gitlab.com/gitlab-com/www-gitlab-com/issues">issues</a> to ask questions.

* [General](/handbook/)
  * [Values](/handbook/values)
  * [General Guidelines](/handbook/general-guidelines)
  * [Handbook usage](/handbook/handbook-usage)
  * [Tools and tips](/handbook/tools-and-tips)
  * [Security Practices](/handbook/security)
  * [Using Git to update this website](/handbook/git-page-update)
* [People Operations](/handbook/people-operations)
  * [Communication](/handbook/communication)
    * [Live streaming](/handbook/live-streaming)
    * [Inclusion & Diversity](/company/culture/inclusion/)
    * [Gender and Sexual-orientation Identity Definitions and FAQ](/handbook/people-operations/gender-pronouns/)
    * [Unconscious bias](/handbook/communication/unconscious-bias/)
    * [Ally resources](/handbook/communication/ally-resources/)
  * [Code of Conduct](/handbook/people-operations/code-of-conduct/)
    * [Anti-Harassment Policy](/handbook/anti-harassment)
  * [Leadership](/handbook/leadership)
  * [Hiring](/handbook/hiring)
    * [Greenhouse](/handbook/hiring/greenhouse)
    * [Vacancies](/handbook/hiring/vacancies)
    * [Interviewing](/handbook/hiring/interviewing)
    * [Jobs FAQ](/jobs/faq)
  * [Onboarding](/handbook/general-onboarding)
  * [Benefits](/handbook/benefits)
    * [Incentives](/handbook/incentives)
    * [Paid time off](/handbook/paid-time-off)
  * [Offboarding](/handbook/offboarding)
  * [Spending Company Money](/handbook/spending-company-money)
    * [Travel](/handbook/travel)
    * [Visas](/handbook/people-operations/visas/)
  * [Secret Snowflake](/handbook/people-operations/secret-snowflake)
* [Engineering](/handbook/engineering/)
  * [Development Department](/handbook/engineering/development/)
    * [CI/CD](/handbook/engineering/development/ci-cd/)
    * [Defend Sub-department](/handbook/engineering/development/defend/)
    * [Dev Sub-department](/handbook/engineering/development/dev/)
    * [Enablement Sub-department](/handbook/engineering/development/enablement/)
    * [Growth Sub-department](/handbook/engineering/development/growth/)
    * [Ops Sub-department](/handbook/engineering/development/ops/)
    * [Secure Sub-department](/handbook/engineering/development/secure/)
  * [Infrastructure Department](/handbook/engineering/infrastructure/)
  * [Quality Department](/handbook/engineering/quality/)
  * [Security Department](/handbook/engineering/security/)
  * [Support Department](/handbook/support/)
    * [Incident Management for Self-Managed Customers](/handbook/support/incident-management/)
  * [UX Department](/handbook/engineering/ux/)
* [Marketing](/handbook/marketing)
  * [Website](/handbook/marketing/website/)
  * [Blog](/handbook/marketing/blog)
  * [Social Media Guidelines](/handbook/marketing/social-media-guidelines)
  * [Marketing and Sales Development](/handbook/marketing/marketing-sales-development/)
    * [Sales Development](/handbook/marketing/marketing-sales-development/sdr/)
    * [Field Marketing](/handbook/marketing/marketing-sales-development/field-marketing/)
    * [Marketing Operations](/handbook/marketing/marketing-sales-development/marketing-operations/)
    * [Marketing Programs](/handbook/marketing/marketing-sales-development/marketing-programs/)
    * [Online Marketing](/handbook/marketing/marketing-sales-development/online-marketing/)
  * [Corporate Marketing](/handbook/marketing/corporate-marketing/)
      * [Content Marketing](/handbook/marketing/corporate-marketing/content/)
  * [Community Relations](/handbook/marketing/community-relations)
  * [Product Marketing](/handbook/marketing/product-marketing/)
  * [Technical Evangelism](/handbook/marketing/technical-evangelism)
    * [Demos](/handbook/marketing/product-marketing/demo/)
  * [Marketing Career Development](/handbook/marketing/career-development/)
* [Sales](/handbook/sales)
  * [Commercial](/handbook/sales/commercial) 
  * [Customer Success](/handbook/customer-success/)
  * [Reseller Channels](/handbook/resellers/)
  * Sales Operations - moved to [Business Operations](/handbook/business-ops)
  * [Reporting](/handbook/business-ops/reporting)
  * [Technical Account Management](/handbook/customer-success/tam/)
* [Finance](/handbook/finance)
  * [Stock Options](/handbook/stock-options)
  * [Board meetings](/handbook/board-meetings)
  * [Business Operations](/handbook/business-ops)
    * [Data Team](/handbook/business-ops/data-team/)
    * [IT Ops Team](/handbook/business-ops/it-ops-team/)
* [Internal Audit](/handbook/internal-audit)
* [Product](/handbook/product)
  * [Release posts](/handbook/marketing/blog/release-posts/)
  * [Making Gifs](/handbook/product/making-gifs)
  * [Data analysis](/handbook/business-ops/data-team/#-data-analysis-process)
  * [Technical Writing](/handbook/product/technical-writing/)
  * [Markdown Guide](/handbook/product/technical-writing/markdown-guide/)
* [Legal](/handbook/legal)
* [Alliances](/handbook/alliances)
* [Acquisitions](/handbook/acquisitions)
* [Changelog](/handbook/CHANGELOG.html)

<style>
.md-page h2 i.icon-color {
  color: rgb(107,79,187)
}
.md-page h2:nth-of-type(even) i.icon-color{
  color:rgb(252,109,38);
}
.font-awesome {
  font-size: .70em;
  vertical-align: middle;
  padding-bottom: 5px;
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 25px;
}
ul.toc-list-icons {
  list-style-type: none;
  padding-left: 25px;
}
ul.toc-list-icons li ul {
  padding-left: 35px;
}
ul.toc-list-icons li i,
ul.toc-list-icons li ul li i {
  padding-right: 15px;
  color: rgb(107,79,187);
}
ul.toc-list-icons li:nth-of-type(even) i {
  color:rgb(252,109,38);
}
</style>
