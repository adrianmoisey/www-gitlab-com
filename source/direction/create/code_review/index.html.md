---
layout: markdown_page
title: "Category Vision - Code Review"
---

- TOC
{:toc}

## Code Review

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas,
and user journeys into this section. -->

Code review is an essential practice of every successful project. It is necessary for achieving the desired level of code quality, and should be characterized it's collaborative and humane nature. In as much as code review protects code quality today, it is also one of the primary avenues of mentorship for an engineer.

GitLab's vision for code review is a place where:

- changes can be discussed,
- developers can be mentored,
- knowledge can be shared, and
- defects identified.

The nature of code review will differ by project (commercial, open source) and contributor (full time, volunteer), but for all contributors code review should be a place to collaborate, grow, and improve so that everyone can contribute great work.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=code%20review)
- [Epic List](https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=code%20review)
- [Overall Vision](/direction/create/)

Interested in joining the conversation for this category? Please join us in our [public epic](https://gitlab.com/groups/gitlab-org/-/epics/688) where we discuss this topic and can answer any questions you may have. Your contributions are more than welcome.

### Background

The code review process involves at least two roles (author, and reviewer) but may involve many people who work together to achieve code quality standards and mentor the author. Furthermore, many reviewers are often not Developers. Reviewers may be Developers, UX Designers, Product Managers, Technical Writers, Security Engineers and more.

In support of GitLab's vision for code review, areas of interest and improvement can be organized by the following goals:

- **efficiency** directly influences velocity within the time span of a single merge request
    - *author efficiency* considers how a merge request author can create and address code review feedback,
    - *reviewer efficiency* considers how an individual reviewer can review a code change, leave feedback, and also verify their own feedback has been addressed,
    - *team efficiency* considers a team can coordinate and communicate responsibilities, progress and status of a merge request, and quickly the entire process can be completed
- **best practices** influence efficiency of teams and projects over a longer time scale, and can include fostering norms and behaviours that aren't explicitly enforced through the application. Amplifying best practices, great defaults and documentation play a significant role in this.
- **love-ability** captures the essence that GitLab is enjoyable to use, which may mean that it is fast, invisible and allows you to get your work done. Particularly, GitLab should encourage the best of communication between colleagues and contributors, helping teams celebrate great contributions of all kinds, and express their ideas without misunderstandings. How GitLab communicates with people, will influence how people communicate with each other inside GitLab.
- **policy** controls that allows code review requirements to be set and enforced, going above and beyond amplifying and encouraging best practice.


## Target Audience and Experience
<!-- An overview of the personas involved in this category. An overview
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

Code review is used by software engineers and individual contributors of all kinds. Depending on their context, however, the workflow and experience of code review can vary significantly.

- **full time contributor** to a commercial product where reducing cycle time is very important. The review cycle is very tight and focussed as a consequence of best practices where keeping merge requests small and iterating at a high velocity are objectives. Code review workflows for these users are **Complete**
- **occasional contributor** to an open source product where cycle time is typically longer as a consequence that they are not working on the project full time. This results in longer review times. When long review times occur, the participants in the merge request will need to spend more time reacquainting themselves with the change. When there are non-trivial amounts of feedback this can be difficult to understand. Code review workflows for these users are **Complete**
- **scientific projects** frequently have a different flow to typical projects, where the development is sporadic, and changes are often reviewed after they have been merged to master. This is a consequence of the high code churn associated with high exploratory work, and having infrequent access to potential reviewers. Post-merge code review workflows are not yet viable in GitLab.

## What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

- **In Progress:** [Smarter merge request diffs using merge refs](https://gitlab.com/groups/gitlab-org/-/epics/854)

    Code reviews are expensive, requiring engineers to carefully review the merge request diff to understand the changes being proposed. The accuracy of the diff is critical for effective code reviews. Additionally, both Atlassian and GitHub have made their diffs smarter, showing the actual difference between the source and target branch, not the source branch and the merge base of the target branch.

- **In Progress:** [Re-review and approval for merge requests](https://gitlab.com/groups/gitlab-org/-/epics/314)

    After a code review, the author will address the feedback with various changes and clarifying questions which the reviewer needs to answer and review before approving. This isn't a lack of trust, but a matter of quality and mentorship so that the feedback is addressed in full and correctly. This _post-review pre-approval workflow_ is currently very difficult. We need to make this better.

- **Next:** [File-by-file merge request diffs](https://gitlab.com/groups/gitlab-org/-/epics/516)

    Diffs are the central place for code review in the merge request interface because that is where the code is actually viewed. Customers evaluating or switching from dedicated code review tools particularly highlight diff navigation, and performance as priorities. Larger merge requests suffer the most currently, both in performance and usability.
    
    A file-by-file review interface will be much faster to load, provide better navigational affordances where the top/bottom of each file are in a predictable location, and scales from small to large merge requests predictably. The single-page scrolling diff interface is well optimized to very simple small merge requests, but becomes cumbersome quickly and manging performance becomes very difficult.

## Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

GitLab competes with both integrated and dedicated code review tools. Because merge requests (which is the code review interface), and more specifically the merge widget, is the single source of truth about a code change and a critical control point in the GitLab workflow, it is important that merge requests and code review in GitLab is excellent. Our primary source of competition and comparison is to dedicated code review tools.

Prospects and new customers, who previously used dedicated code review tools typically have high expectations and accustomed to a high degree of product depth. Given that developers spend a significant portion (majority?) of their in application time in merge requests, limitations are quickly noticed and become a source of frustration.

Integrated code review packaged with source code management:

- [Phabricator](https://www.phacility.com/phabricator/) by Phacility (**very mature**) ([example](https://phabricator.haskell.org/D4953))
- [Gerrit](https://www.gerritcodereview.com/index.html) _(free, open source)_ (**very mature**)
- [GitHub](https://github.com/features/code-review/)
- [Bitbucket](https://bitbucket.org/product/features) by Atlassian
- [Azure DevOps](https://azure.microsoft.com/en-us/services/devops/) by Microsoft

Dedicated code review tools:

- [Crucible](https://www.atlassian.com/software/crucible) by Atlassian  (**very mature**) ([Bitbucket vs Crucible](https://confluence.atlassian.com/bitbucketserverkb/what-s-the-difference-between-crucible-and-bitbucket-server-do-i-need-both-779171640.html))
- [Review Board](https://www.reviewboard.org/) ([example](http://demo.reviewboard.org/r/844/diff/1/#index_header))
- [Reviewable](https://reviewable.io/) ([example](https://reviewable.io/reviews/Reviewable/demo/1))

## Market Research
<!-- This section should link or highlight any relevant market research you've done that justifies our
entry into the market for the particular category. -->

Estimates place the version control systems market at USD 438 million in 2018, with forecast growth to USD 716 million by 2023 ([source](https://www.prnewswire.com/news-releases/global-version-control-systems-market-report-2018---forecast-to-2023-ebay-using-micro-focus-software-for-continuous-process-improvement-300669378.html)). Most Git hosting vendors (peers) offer integrated code review capabilities (GitHub, Bitbucket, Phabricator) and these are by far the most commonly used code review tools among current prospects and recent customers.

The dedicated code review tools market is much smaller, and the largest maturest dedicated product is bundled by Atlassian with rest of its suite.

## Business Opportunity
<!-- This section should highlight the business opportunity highlighted by the particular category. -->

Code Review and Source Code Management are typically seen as one and the same by most customers, particularly prospects already using an integrated product like GitHub. GitLab is competitive against it's peers, but this continues to be a well contested aspect of the source code management market, which is unsurprising given developers typically spend the majority of their time in a tool like GitLab performing code reviews or addressing code review feedback.

## Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

The highest priority customer requests are for improved application performance, accuracy and efficiency for reviewing merge request diffs of all sizes, small and extremely large.

- [Smarter merge request diffs using merge refs](https://gitlab.com/groups/gitlab-org/-/epics/854) address accuracy problems in some situations, thereby improving **efficiency** of reviews by showing the expected diff contents.
- [File-by-file merge request diffs](https://gitlab.com/groups/gitlab-org/-/epics/516) improves application performance, thus improving **reviewer efficiency** by reducing the time waiting to view diffs.
- [Track unread diffs, files, and discussions](https://gitlab.com/groups/gitlab-org/-/epics/1409) improves usability. primarily improves **reviewer efficiency** by allowing reviews to be performed incrementally over multiple sittings, and better handling the iterative process of leaving feedback and the author proposing improvements.

The other important category of customer requests, that less urgent but requested are for improved workflow efficient in environments with complex approval policies:

- [Per branch merge request approval rules](https://gitlab.com/gitlab-org/gitlab-ee/issues/460) improves **efficiency** in strict policy environments by allowing restrictive controls for some target branches, and permissive controls for others.
- [Merge request approval chains](https://gitlab.com/gitlab-org/gitlab-ee/issues/965) improves **efficiency** within restrictive control flows by allowing organizational hierarchy to be reflected in the merge chain, allowing incremental escalation of a merge request from a broad pool of more junior reviewers, up to the most senior reviewer.

Other notable requests include:

- [Cross-project code review (group merge requests)](https://gitlab.com/groups/gitlab-org/-/epics/457)
- [Post-merge code review](https://gitlab.com/groups/gitlab-org/-/epics/872) is  of interest to a variety of organizations where changes are merged with a very high velocity (e.g daily) and they desire to review aggregate set of changes semi-regularly.

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

- [Smarter merge request diffs using merge refs](https://gitlab.com/groups/gitlab-org/-/epics/854)
- [Per branch merge request approval rules](https://gitlab.com/gitlab-org/gitlab-ee/issues/460)

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

The top internal customer feedback is highly aligned with top customer feedback, with top feedback relation to improving the speed of loading and navigating diffs, and keep track of what has been reviewed.

- [File-by-file merge request diffs](https://gitlab.com/groups/gitlab-org/-/epics/516)
- [Track unread diffs, files, and discussions](https://gitlab.com/groups/gitlab-org/-/epics/1409)

Other important priorities include:

- [Improve unresolved discussion navigation](https://gitlab.com/groups/gitlab-org/-/epics/1403) will help improve navigating discussions as merge requests grow in size and age, and become more difficult for reviewers and approvers
- [Merge request reviewers](https://gitlab.com/groups/gitlab-org/-/epics/1330) improves **team efficiency** by improving hand off between team members, and making it clear the different roles of participation in a merge request.
- [Increased focus of merge request changes tab](https://gitlab.com/groups/gitlab-org/-/epics/1406) will make code review more **love-able** by reducing distraction, and making use of the navigational affordances of the top of the page which is quickly accessed by mouse and keyboard.

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

It is critical the merge request is reliably fast, accurate and efficient, as it is a blocking step in the software development lifecycle. It is important that GitLab smooth and streamline the process, and not become a source of friction or frustration to the adoption of best practice or high velocity.

- [File-by-file merge request diffs](https://gitlab.com/groups/gitlab-org/-/epics/516) primarily improves **reviewer efficiency** be allow faster access to each file, and improved usability for large diffs.
- [Track unread diffs, files, and discussions](https://gitlab.com/groups/gitlab-org/-/epics/1409) primarily improves **reviewer efficiency** by allowing reviews to be performed incrementally over multiple sittings, and better handling the iterative process of leaving feedback and the author proposing improvements.

Commits are the critical unit of work in Git, and used well are tremendously valuable. GitLab should be enabling and amplifying best practice behaviours using commits, so that the commit messages and each commit is a valuable asset for years. Great commit messages and meaningful commits allow developers to move quickly, and effectively make use of the entire ecosystem of Git tooling.

- [Commit messages as part of code review](https://gitlab.com/groups/gitlab-org/-/epics/286) helps teams adopt **best practice** commit messages by making them reviewable. Commit messages are part of the distributed nature of Git and should used to maximum extent so that valuable information is portable and easily accessed through local Git tooling.
- [Commit by commit code review](https://gitlab.com/groups/gitlab-org/-/epics/285) will teams that perform commit by commit reviews, which are a **best practice** for some teams.

Coordination of responsibilities and handoff can be time consuming, particularly for changes spanning backend, frontend, database and perhaps other services. Tools to make requesting a code review, understand the progress of the code review, and hand off from reviewer back to engineer is important.

- [Merge request reviewers](https://gitlab.com/groups/gitlab-org/-/epics/1330) improves **team efficiency** by improving hand off between team members, and making it clear the different roles of participation in a merge request.

