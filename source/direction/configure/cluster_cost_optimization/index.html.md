---
layout: markdown_page
title: "Category Vision - Cluster Cost Optimization"
---

- TOC
{:toc}

## Cluster Cost Optimization

Compute costs is a significant expenditure for many companies, whether they are in the cloud or on-premise. Managing these costs is an important function for many companies, frequently with dedicated employees to do so.
There are two main goals:

Driving increases in efficiency, to save money
Attribute costs to individual projects, then groups

GitLab is well positioned to provide a comprehensive solution here:

We know the performance characteristics of each pod and node through our Prometheus integration.
We know the resources requested by each pod
We know which pods correspond to which projects. This is important for shared clusters, which offer the greatest opportunities for efficiency gains due to scale.
We can even track changes in usage to individual deploys.

There are a few iterations we could go through here:

Flag deployments/projects that overprovisioned resource requests, wasting resources
Recommend node changes to increase efficiency (use larger nodes, nodes with more RAM, etc.)
Estimate costs utilized by each project
Automatically implement the changes to right-size pod resource requests

This feature could pay for the GitLab Ultimate license by itself pretty quickly, if we do it well, for companies at certain scale.


- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Cluster%20Cost%20Optimization)
- [Overall Vision](/direction/configure)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/595)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/503) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's next & why

[Flag over-provisioned kubernetes deployments](https://gitlab.com/gitlab-org/gitlab-ee/issues/9049), as we have the required data sources to provide this information.

## Competitive landscape

### Kubecost
[kubecost.com](https://www.kubecost.com)

### CloudHealth Technologies
[cloudhealthtech.com/blog/kubernetes-cost-allocation-made-easy](https://www.cloudhealthtech.com/blog/kubernetes-cost-allocation-made-easy)

### Replex
[replex.io/blog/kubernetes-cost-allocation-in-a-nutshell](https://www.replex.io/blog/kubernetes-cost-allocation-in-a-nutshell)

### Supergiant
[supergiant.io/blog/supergiant-packing-algorithm-unique-save-money](https://supergiant.io/blog/supergiant-packing-algorithm-unique-save-money)

### Mist.io
[mist.io](https://mist.io/)

### Cloudability
[cloudability.com](https://www.cloudability.com/)

## Analyst landscape

TBD

## Top Customer Success/Sales issue(s)

TBD

## Top user issue(s)

TBD

## Top internal customer issue(s)

TBD

## Top Vision Item(s) 

[Flag over-provisioned kubernetes deployments](https://gitlab.com/gitlab-org/gitlab-ee/issues/9049)