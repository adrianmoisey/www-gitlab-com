---
layout: markdown_page
title: "Category Vision - Chaos Engineering"
---

- TOC
{:toc}

## Chaos Engineering

Chaos engineering is the discipline of experimenting on a software system in production in order to build confidence in the system’s capability to withstand turbulent and unexpected conditions.

We want to safely allow operators to run downtime scenarios in pre-prod environments randomly. Starting with the min unit (pod) all the way to the max unit (cluster).
Once operators have built/configured a good fail-over plan, allow then to run downtime scenarios in production environments.
Provide relevant metrics alongside incidents.


- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=chaos%20engineering)
- [Overall Vision](/direction/configure)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/595)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/381) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

[Add Kube Monkey to GitLab-managed k8s apps](https://gitlab.com/gitlab-org/gitlab-ce/issues/58403)

GitLab has used Kube Monkey as part of testing our helm charts. kube-monkey is an implementation of Netflix's Chaos Monkey for Kubernetes clusters. It randomly deletes Kubernetes (k8s) pods in the cluster encouraging and validating the development of failure-resilient services.

## Competitive landscape

### Gremlin
Gremlin provides a framework to safely, securely, and easily simulate real outages with an ever-growing library of attacks.

### ChaosToolkit
Chaos Toolkit is a project whose mission is to provide a free, open and community-driven toolkit and API to all the various forms of chaos engineering tools that the community needs.

## Analyst landscape

TBD

## Top Customer Success/Sales issue(s)

TBD

## Top user issue(s)

TBD

## Top internal customer issue(s)

[Add Kube Monkey to GitLab-managed k8s apps](https://gitlab.com/gitlab-org/gitlab-ce/issues/58403)

## Top Vision Item(s) 

[Add Kube Monkey to GitLab-managed k8s apps](https://gitlab.com/gitlab-org/gitlab-ce/issues/58403)