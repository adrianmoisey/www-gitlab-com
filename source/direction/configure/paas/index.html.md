---
layout: markdown_page
title: "Category Vision - PaaS"
---

- TOC
{:toc}

## PaaS

Platform-as-a-Service (PaaS) is a category of cloud computing services that provides a platform allowing customers to develop, run, and manage applications without the complexity of building and maintaining the infrastructure typically associated with developing and launching an app.

The advantages of PaaS are primarily that it allows for higher-level programming with dramatically reduced complexity; the overall development of the application can be more effective, as it has built-in/self up-and-down ramping infrastructure resources; and maintenance and enhancement of the application is thus easier.

An important point on our [pricing strategy](https://about.gitlab.com/strategy/#pricing):

> We charge for making people more effective and will charge per user, per application, or per instance. We do include free minutes with our subscriptions and trials to make it easier for users to get started. As we look towards more deployment-related functionality on .com it's tempting to offer compute and charge a percent on top of, for example, Google Cloud Platform (GCP). We don't want to charge an ambiguous margin on top of another provider since this limits user choice and is not transparent. So we will always let you BYOK (bring your own Kubernetes) and never lock you into our infrastructure to charge you an opaque premium on those costs.

### Problem to solve

**Remove the complexity of provisioning and maintaining the infrastructure necessary for developing and launching an application.**

These complexity and concerns stem mostly from scale, cost, security, and scalability. Configurability and customization are also important and as Kubernetes and Knative mature, more options will be available for operators to alleviate these concerns.

### Considerations

- Must ensure security for prod ready applications
- Must avoid use of privileged docker images, no root use
- Use kaniko to build containers and avoid docker-in-docker
- Resources should idle when not in use for max efficiency


- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=PaaS)
- [Overall Vision](/direction/configure)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/595)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/111) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's next & why

Option to designate PaaS Grade (Secure) Cluster https://gitlab.com/gitlab-org/gitlab-ce/issues/59665

In order to take advantage of the features offered by both Kubernetes and Knative, we must provide an avenue for all users of an instance to use these resources. To make it secure and scalable, these policies must be applied at the top level.

Focusing on security and isolation features will serve users who are interested in doing hard multi-tenancy and have higher requirements for security.

Now that [instance-level clusters](https://gitlab.com/gitlab-org/gitlab-ce/issues/39840) are available, we will work on scalability and security:

1. Provide the ability to further secure an instance-level cluster by installing a CRD/Operator which will will enforce all svc accounts are scoped to operate in the CRD [https://gitlab.com/gitlab-org/gitlab-ce/issues/59665](https://gitlab.com/gitlab-org/gitlab-ce/issues/59665)
1. Isolate containers running in the cluster to reduce the risk of breaking out to the host. Use gVisor to create annotated Knative services [https://gitlab.com/gitlab-org/gitlab-ce/issues/59666](https://gitlab.com/gitlab-org/gitlab-ce/issues/59666)

Once security and scalability are in place, we'll work to provide gitlab.com users a great PaaS experience

1. Create an instance-level cluster for gitlab.com in order to launch PaaS to a limited group of beta users [https://gitlab.com/gitlab-org/gitlab-ce/issues/59668](https://gitlab.com/gitlab-org/gitlab-ce/issues/59668)

## Competitive landscape

### Heroku
Heroku is a cloud platform as a service supporting several programming languages. Heroku, one of the first cloud platforms, has been in development since June 2007, when it supported only the Ruby programming language, but now supports Java, Node.js, Scala, Clojure, Python, PHP, and Go.

### Cloud Foundry
Cloud Foundry is an open source, multi-cloud application platform as a service governed by the Cloud Foundry Foundation, a 501 organization. The software was originally developed by VMware and then transferred to Pivotal Software, a joint venture by EMC, VMware and General Electric. Its hosted service is called Pivotal Web Services, it bills itself as a modern runtime for Spring Boot, .NET*, and Node apps

### Google App Engine
Google App Engine is a web framework and cloud computing platform for developing and hosting web applications in Google-managed data centers. Applications are sandboxed and run across multiple servers.

### AWS Lambda

AWS Lambda lets you run code without provisioning or managing servers. You pay only for the compute time you consume - there is no charge when your code is not running. With Lambda, you can run code for virtually any type of application or backend service - all with zero administration. Just upload your code and Lambda takes care of everything required to run and scale your code with high availability.

### Azure Functions

Azure Functions is an event driven, compute-on-demand experience that extends the existing Azure application platform with capabilities to implement code triggered by events occurring in Azure or third party service as well as on-premises systems.

## Analyst landscape

TBD

## Top Customer Success/Sales issue(s)

TBD

## Top user issue(s)

TBD

## Top internal customer issue(s)

[Add an instance wide cluster to GitLab.com](https://gitlab.com/gitlab-org/gitlab-ce/issues/59668)

## Top Vision Item(s)
[Option to designate PaaS Grade (Secure) Cluster](https://gitlab.com/gitlab-org/gitlab-ce/issues/59665)