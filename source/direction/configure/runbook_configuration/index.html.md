---
layout: markdown_page
title: "Category Vision - Runbook Configuration"
---

- TOC
{:toc}

## Runbook Configuration


Runbooks are a collection of documented procedures that explain how to carry out a particular process, be it starting, stopping, debugging, or troubleshooting a particular system.

Historically, runbooks took the form of a decision tree or a detailed step-by-step guide depending on the condition or system.

Modern implementations have introduced the concept of an “executable runbooks”, where, along with a well-defined process, operators can execute pre-written code blocks or database queries against a given environment.

We aim to extend Incident Management further by rendering runbooks inside GitLab as interactive documents for operators; link from incident management screens so when an incident happens, we point you to the relevant runbook. Additionally, we want to allow runbooks to trigger ChatOps functions defined in gitlab-ci.yml.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=runbooks)
- [Overall Vision](/direction/configure)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/595)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/380) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's next & why

[Repository integration for JupyterHub](https://gitlab.com/gitlab-org/gitlab-ce/issues/47138)

With security in place we now want to allow users to easily access the contents of their repository to easily build runbooks.

## Competitive landscape

While there are some offerings in the marketplace, they rely on heavy customization for every use case and are somewhat specialized (ie networking).

### VictorOps

VictorOps is an unified incident management platform that offers real-time alerting, collaboration, and documentation solutions for DevOps teams.

### NetBrain

NetBrain technologies provides map-driven network automation solution. 

## Analyst landscape

TBD

## Top Customer Success/Sales issue(s)

TBD

## Top user issue(s)

[Group-level filtering for JupyterHub deployment](https://gitlab.com/gitlab-org/gitlab-ce/issues/52536)

## Top internal customer issue(s)

TBD

## Top Vision Item(s) 

[Runbook Automation](https://gitlab.com/gitlab-org/gitlab-ee/issues/3911)