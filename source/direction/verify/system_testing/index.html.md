---
layout: markdown_page
title: "Category Vision - System Testing"
---

- TOC
{:toc}

## System Testing

Modern software is often delivered as a collection of (micro)services to multiple clouds, rather than a single monolith to your own data center. Validating complex interactions/compatibility in order to ensure the reliability of the system as a whole is more important than ever.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3A%3ASystem%20Testing)
- [Overall Vision](https://about.gitlab.com/direction/verify)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/592)

Interested in joining the conversation for this category? Please join us in our
[public epic](https://gitlab.com/groups/gitlab-org/-/epics/1306) where
we discuss this topic and can answer any questions you may have. Your contributions
are more than welcome.

## What's Next & Why

Next up will be a proof of concept of using Docker Compose to run integration tests for a project or group of projects
that consists of interdependent microservices as part of [gitlab-ce#22559](https://gitlab.com/gitlab-org/gitlab-ce/issues/22559).
From there, we will work with our internal customer to understand what, if any problems that solves with our current use of
[`gitlab-qa`](https://gitlab.com/gitlab-org/gitlab-qa/blob/master/docs/what_tests_can_be_run.md#orchestrated-tests)

We're also looking at compatibility testing via [gitlab-ee#6061](https://gitlab.com/gitlab-org/gitlab-ee/issues/6061), which
will introduce Selenium integration to capture multi-browser testing results as well as screenshots and display them in a
CI view. This will start the ball rolling forward on how we can better support these kinds of testing needs.

## Maturity Plan

This category is currently at the "Planned" maturity level, and
our next maturity target is Minimal (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).
Key deliverables to achieve this are:

- [Use Docker image registry and Docker Compose to run cross-project integration tests within single project's pipeline](https://gitlab.com/gitlab-org/gitlab-ce/issues/22559)
- [Group level Review Apps](https://gitlab.com/gitlab-org/gitlab-ce/issues/58600)
- [Automatic multi-cloud validation MVC](https://gitlab.com/gitlab-org/gitlab-ee/issues/7982)

It is possible we will choose one of the two above items to reach minimal level, depending on how user research plays out.

## Competitive Landscape

No other CI platforms provide first-party compatibility or system testing, but all do provide different kinds of integrations.
Selenium is a very popular one, and we plan to add a CI view for it via [gitlab-ee#6061](https://gitlab.com/gitlab-org/gitlab-ee/issues/6061).
SauceLabs ([gitlab-ce#22755](https://gitlab.com/gitlab-org/gitlab-ce/issues/22755)) is also a popular testing lab that provides
devices for ensuring compatibility across a spectrum of browsers and/or devices.

### API testing tools

API testing tools like [SoupUI Pro](https://www.soapui.org/professional/soapui-pro.html), [Apache JMeter](https://jmeter.apache.org/)
and [Hoverfly](https://hoverfly.io/) allows users to automate API and microservices testing across multiple services.

## Top Customer Success/Sales Issue(s)

There are no top CS/Sales issues for this category.

## Top Customer Issue(s)

Customers have requested we implement WebAssembly testing ([gitlab-ce#58212](https://gitlab.com/gitlab-org/gitlab-ce/issues/58212)) for system testing.

## Top Internal Customer Issue(s)

Apart from the CI view support for Selenium mentioned in the competitive landscape section, [gitlab-ce#22755](https://gitlab.com/gitlab-org/gitlab-ce/issues/22755)
(integration support for SauceLabs) is also requested. [team-tasks#45](https://gitlab.com/gitlab-org/quality/team-tasks/issues/45)
tracks the progress of the internal quality team at GitLab rolling out compatibility testing.

## Top Vision Item(s)

The top vision issue for this category is [group-level review apps](https://gitlab.com/gitlab-org/gitlab-ce/issues/58600).

Adding CI view support for Selenium ([gitlab-ee#6061](https://gitlab.com/gitlab-org/gitlab-ee/issues/6061)) is the most
important vision item for the same reasons as in the competitive landscape above.
