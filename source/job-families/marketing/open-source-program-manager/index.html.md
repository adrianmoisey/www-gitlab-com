---
layout: job_family_page
title: "Open source program manager"
---

As the Open Source Program Manager, you will be responsible for growing the number of Open Source projects and developers that adopt GitLab.

## Responsibilities

- Identify and cultivate strategic relationships with current and potential partners in the context of leading projects and organizations in the Open Source space.
- Enable large Open Source projects to move to GitLab. Set expectations, provide them support, training, advice and help them manage the migration process.
- Develop and execute the collaboration strategy for key projects, as well as co-marketing campaigns and communication plans.
- Act as a liaison between internal teams at GitLab and strategic partners to communicate the product roadmap, feature requests, and to help prioritize them.
- Run and further develop the [GitLab Open Source Initiative](https://about.gitlab.com/solutions/open-source) to increase the overall number of Open Source projects that move to GitLab.


## Requirements

* You have 5-7 years of experience running developer relations or community advocacy programs, preferably open source and technical in nature.
* Analytical and data driven in your approach to building and nurturing communities.
* You have experience facilitating sensitive and complex community situations with humility, empathy, judgment, tact, and humor.
* Excellent spoken and written English.
* Familiarity with developer tools, Git, Continuous Integration, Containers, Kubernetes and Cloud Native technologies in general.
* You share our [values](/handbook/values/), and work in accordance with those values.
* Relationships in prominent Open Source communities are a plus.








